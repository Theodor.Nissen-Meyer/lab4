package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;


public class CellPositionToPixelConverter {
  // TODO: Implement this class
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }
  public Rectangle2D getBoundsForCell(CellPosition cp){
    int rows = gd.rows();
    int cols = gd.cols();

    double cellWidth = (box.getWidth() - (this.margin * (cols +1))) / cols;                 // finner cellevidden utifra bredden til bakgrunnen, delt på antall kolonner
    double cellHeight = (box.getHeight() - (this.margin * (rows +1))) / rows;               // finner cellebredden på samme måte

    double cellX = box.getX() + (this.margin * (cp.col() + 1)) + (cellWidth * cp.col());    // Finner x- og y-verdi 
    double cellY = box.getY() + (this.margin * (cp.row() + 1)) + (cellHeight * cp.row());

    Rectangle2D cell = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);         // Cellens egne koordinater returneres
    return cell; 
  }

}
