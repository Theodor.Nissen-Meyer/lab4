package no.uib.inf101.gridview;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;



public class GridView extends JPanel{
  IColorGrid iColorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;


  // TODO: Implement this class
  public GridView(IColorGrid iColorGrid){
    this.iColorGrid = iColorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);
    
    
  }
  
  public void drawGrid(Graphics2D g2){
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
  
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    
    Rectangle2D bg = new Rectangle2D.Double(x, y, width, height);       // Grå bakgrunn med x-,y-verdi, og høyde og bredde
    g2.setColor(MARGINCOLOR);
    g2.fill(bg);
    
    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(bg, iColorGrid, OUTERMARGIN);     // Finner koordinatene til colorgriddet ut i fra bakgrunnen og marginen

    drawCells(g2, iColorGrid, converter);
    
  }
  
  private static void drawCells(Graphics2D g2, CellColorCollection colors, CellPositionToPixelConverter converter){

    List<CellColor> list = colors.getCells();     // henter fargene i en liste
    for (CellColor cell : list) {
      Rectangle2D rectangle = converter.getBoundsForCell(cell.cellPosition());    // oppretter rektangler for hver celle, finner koordinatene til hver celle
      Color color = cell.color();                                                 // henter fargen
      if (color == null){                                                         // Hvis fargen er null blir den mørk grå isteden
        color = Color.DARK_GRAY;
      }
      g2.setColor(color);
      g2.fill(rectangle);
      
    }
    
  }

  
}
