package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;

import javax.swing.JFrame;


public class Main {
  public static void main(String[] args) {

    JFrame frame = new JFrame();
    
    IColorGrid grid = new ColorGrid(3, 4);            // Nytt grid med 3 og 4
    grid.set(new CellPosition(0, 0), Color.RED);        // fargene som skal tegnes i rutenettet
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    
    GridView view = new GridView(grid);                          // Lager gridveiw objekt med griddet
    
    frame.setTitle("INF101");
    frame.setContentPane(view);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

  }
}
