package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


public class ColorGrid implements IColorGrid{
  public int rows;
  private int cols;
  List <List<Color>> grid;

  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();

    for (int i = 0; i < rows; i++) {
      this.grid.add(new ArrayList<>());
      for (int j = 0; j < cols; j++) {
        List<Color> row = this.grid.get(i);
        row.add(null);
      }
    }
  }

  @Override
  public int rows() {
    // TODO Auto-generated method stub
    return this.rows;
  }

  @Override
  public int cols() {
    // TODO Auto-generated method stub
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<CellColor>();
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        CellPosition pos = new CellPosition(i, j);
        Color color = get(pos);
        cells.add(new CellColor(pos, color));
      }
    }
    return cells;

  }
    // TODO Auto-generated method stub

  @Override
  public Color get(CellPosition pos) {                    // Returnerer en farge fra rad- og kolonnenummeret til en Celle
    List<Color> row = grid.get(pos.row());                // Raden hentes fra griddet gitt i Main og opprettet i Konstruktøren
    Color color = row.get(pos.col());
    return color;
  }
  

  @Override
  public void set(CellPosition pos, Color color) {        // Plasserer en farge i en gitt celle
    int row = pos.row();
    int col = pos.col();

    List<Color> thisRow = grid.get(row);
    thisRow.set(col, color);
    
  }
  // TODO: Implement this class
}
